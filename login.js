var Vue = require("./vue.min.js");
var axios = require("axios");

var nav = new Vue({
  el: '#loginForm',
  data: {
    mail: "",
    pass: "",
    state: "",
    rank: 0
  },
  methods: {
  	login: function(){
  		console.log({
			lic: nav.mail,
			pass_: nav.pass
		});
  		axios.post("https://27b6fmf936.execute-api.us-west-2.amazonaws.com/stage", {
			lic: nav.mail,
			"pass_": nav.pass,
			"type_": "check",
    		"rank_": ""
		}, {
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		})
		.then(function (res_) {
			console.log(res_.data);
			var _data = res_.data[0];
			console.log(_data);
			if(_data == "login ok"){
				nav.rank = res_.data[1];
				writeCookie();
			}
			if(_data == "err - time"){
				nav.state = "数分以内にログインがありました。１０分ほど開けてからログインをお願いします。"
			}
			if(_data == "err - More than one key matches"){
				nav.state = "メールアドレスまたは、パスワードが一致しません。"
			}
		})
		.catch(function (err_) {
			console.log(err_);
		});
  	}
  }
});

function writeCookie(){
	console.log("...");
	axios.post("http://18.144.53.147/okLogin", {
		"adress": nav.mail,
		"rank": nav.rank
	})
	.then(function (res_) {
		var _data = res_.data;
		if(_data == "ok"){
			location.href = "http://18.144.53.147";
		}
	})
	.catch(function (err_) {
		writeCookie();
	});
}