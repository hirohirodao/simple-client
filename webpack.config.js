var path = require('path');
var webpack = require('webpack');
module.exports = [
  {
    entry: ['./main.js'],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    externals: [],
  },
];

// var path = require('path');
// var webpack = require('webpack');
// module.exports = [
//   {
//     entry: ['./login.js'],
//     output: {
//       filename: 'login.js',
//       path: path.resolve(__dirname, 'dist')
//     },
//     externals: [],
//   },
// ];