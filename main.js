var Vue = require("./vue.min.js");
var axios = require("axios");

var nav = new Vue({
  el: '#navId',
  data: {
    loginName: "getting...",
    rank: 0
  }
});

var lists = [];

var app = new Vue({
  el: '#app',
  data: {
    datas: [],
    sites: {}
  },
  methods: {
  	jump: (val_) => {
  		window.open('/jump/'+val_, '_blank');
  	}
  }
});

function getArgment(){
	axios.get("./argment")
	.then(function (res_) {
		// console.log(res_.data);
		nav.loginName = res_.data[0];
		nav.rank = res_.data[1];
		getRanks();
		activate();
	})
	.catch(function (err_) {
		getArgment();
		console.log(err_);
	});
}

function getRanks(){
	axios.get("./list")
	.then(function (res_) {
		// console.log(res_.data);
		list = res_.data;
		get();
	})
	.catch(function (err_) {
		getArgment();
		console.log(err_);
	});
}

function get(){
	axios.get("./data")
	  .then(function (res_) {
	  	var _data = res_.data;
	  	var _num = 0;
	  	app.datas = [];
	  	for(var i = 0; i < _data.length / 12; i++){
	  		// console.log(app.datas);
	  		var _ary = [];
	  		for(var j = 0; j < 12; j++){
	  			_ary[j] = _data[_num];
	  			_num++;
	  		}
  			if(list[_ary[3]].rank <= nav.rank){
  				if(list[_ary[7]].rank <= nav.rank){
  					app.datas.push(_ary);
  				}
  			}
	  	}
	  	setTimeout(function(){
	  		get();
	  	}, 2000);
	  	// console.log(app.datas);
	  })
	  .catch(function (err_) {
	    console.log(err_);
	    get();
	  });
}

getArgment();

function activate(){
	axios.post("https://27b6fmf936.execute-api.us-west-2.amazonaws.com/stage", {
		"lic": nav.loginName,
		"type_": "active",
		"pass_": "NULL",
		"rank_": 3
	})
	.then(function (res_) {
		console.log(res_.data);
		setTimeout(function(){
			activate();
		}, 28000);
	})
	.catch(function (err_) {
		activate();
	});
}